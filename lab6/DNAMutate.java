//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Samantha Valera and Gabriel Herbst
// CMPSC 111 Spring 2017
// Lab # 6
// Date: Feburary 23 2017
//
// Purpose: Mutating DNA by inserting, deleting and switching out random chars in a string that the user inputs.
//***********************
import java.util.*; 



public class DNAMutate
{
	//------------------
	// main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
		//Label output with name and date:
		System.out.println("Samantha Valera and Gabriel Herbst \nLab #6");
		System.out.println(new Date());
		System.out.println("");

	Scanner scan = new Scanner(System.in);
	Random r = new Random();
	
	int length, location;

	String dnaString; //original input from user
        String Mutation1; //Uppercase version of input from user
	String Compliment;//Compliment to input from user
	String Mutation2; //Uppercase version of compliment strand
	String Mutation3;//Random insertion into dnaString
	String s = "ATGC"; //String with all available nucleotides
	String Mutation4; //Random deletion variable
	String Mutation5; //Random switch variable


		System.out.println("Enter a string containing only C, G, T, A");
		dnaString = scan.nextLine(); //Assigns user input to variable
		Mutation1 = dnaString.toUpperCase();

		System.out.println("The string you entered is " + Mutation1);
	/* replacing input from user with compliment nucleotides 
		and then converting to uppercase*/
		Compliment = Mutation1.replace('A','t');
		Compliment = Compliment.replace('T','a');
		Compliment = Compliment.replace('G','c');
		Compliment = Compliment.replace('C','g');
		Mutation2 = Compliment.toUpperCase();

	System.out.println("The compliment is " + Mutation2);

	length = dnaString.length(); 
	location = r.nextInt(length+1); //generates a random location in the string inputted from user
	char c = s.charAt(r.nextInt(4)); //picks a random character from the allowed dna nucleotide letters
	Mutation3 = Mutation1.substring(0,location) + c + Mutation1.substring(location); /*inputs a random character "c" in a random location
											 "location" in the string inputted from user*/
	System.out.println("");

	System.out.println("Randomly inserting nucleotide " + c + " at location " + location + " gives: " + Mutation3);
	
	

	location = r.nextInt(length); //generates a random location
if (location == 0) 
	Mutation4 = Mutation1.substring(1,length); // If random location = 0, Mutation 4 is the substring of Mutation1 from char 1 to the length
else
	Mutation4 = Mutation1.substring(0,location-1) + Mutation1.substring(location,length); /*Otherwise, Mutation 4 = the substring of Mutation1 
					from 0 to the char before location concatenated with substring from location to the end of the string*/
  System.out.println("");
  System.out.println("Deleting a nucleotide from location " + location + " gives: " + Mutation4);


location = r.nextInt(length); //generates a random location
char e = s.charAt(r.nextInt(4)); //picks a random char from String s
char d = Mutation1.charAt(r.nextInt(length)); //picks a random char from the user input
Mutation5 = Mutation1.replace(d, e); //replaces the random char from user input with random char in string s
System.out.println("");
System.out.println("Replacing nucleotide " + d + " at position " + location + " with " + e + " gives: " + Mutation5);







	}
}
