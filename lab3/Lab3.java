//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Samantha Valera
// CMPSC 111 Spring 2017
// Lab #3
// Date: February 02 2017
//
// Purpose: To create a bill calculator.
//***********************
import java.util.Scanner;

public class Lab3
{
	public static void main(String[] args)
	{
		System.out.println("Samantha Valera");
 		System.out.println("Lab 3"); 
		
		String name;
		double bill;
		double percentage;
		int d=100;	//Used for when you divide percentage
		int people; //People paying the bill
		double pay; //What each person will pay
		double tip;
		double total;

		Scanner scan = new Scanner(System.in);
	
		System.out.println("Please enter your name: ");
		name = scan.nextLine(); 
			
		System.out.println("Hello " + name); 
		
		System.out.println("Please enter your bill: ");
		bill = scan.nextDouble();

		System.out.println("Please enter the percentage you want to tip: ");
		percentage = scan.nextDouble();

		System.out.println("Your original bill was $" + bill);
		
		tip = (percentage / d) * bill; 

		System.out.println("Your tip amount is $" + tip); 
			
		total = bill + tip;

		System.out.println("Your total amount is $" + total);
	
		System.out.println("How many people will be splitting the bill? ");
		people = scan.nextInt();
		
		pay = total / people; 
		
		System.out.println("Each person should pay $" + pay);

		System.out.println("Thank you, have a nice day!");
	}
}
