//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Samantha Valera
// CMPSC 111 Spring 2017
// Practical02
// Date: January 27 2017
//
// Purpose: To print a sunflower.
//***********************
import java.util.Date;
public class Practical02
{
	public static void main(String[] args)
	{
		System.out.println("Samantha Valera\n" + new Date() + "\n");
		System.out.println("    	 ^ ^");
		System.out.println("       <  o  >");
		System.out.println("	 v v");
		System.out.println("	 | |");
		System.out.println("	 | |");
		System.out.println("       |_____|");
		System.out.println("	|   |"); 
		System.out.println("	|   |");	
		System.out.println("	|___|");
	}
}
