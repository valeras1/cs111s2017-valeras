//*****************************
// CMPSC 111
// Practical 8
// October 30, 2015
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;

public class YearCheckerMain
{
    public static void main ( String[] args)
    {
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;

        System.out.print("Please enter a year between 1000 and 3000: ");
        userInput = scan.nextInt();
	
        YearChecker activities = new YearChecker(userInput);

	//Leap Year
	if (activities.isLeapYear() == true)
		System.out.println("This is a leap year.");
	else
	{
		System.out.println("This is not a leap year.");
	}
	//Cicada Year
	if (activities.isCicadaYear() == true)
		System.out.println("This is a cicada year.");
	else
	{
		System.out.println("This is not a cicada year.");
	}
	//Sunspot Year
	if (activities.isSunspotYear() == true)
		System.out.println("This is a sunspot year.");
	else
	{
		System.out.println("This is not a sunspot year.");
	}
        System.out.println("Thank you for using this program.");
    }
}
