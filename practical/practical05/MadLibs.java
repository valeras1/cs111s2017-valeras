//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Samantha Valera
// CMPSC 111 Spring 2017
// Practical 5
// Date: February 24 2017
//
// Purpose: Create a mad lib
//***********************
import java.util.Date; 
import java.util.Scanner;

public class MadLibs
{
	//------------------
	// main method: program execution begins here
	//------------------
	public static void main(String[] args)
	{
		System.out.println("Samantha Valera Practical 05"); 
		System.out.println(new Date());

		Scanner scan = new Scanner(System.in);

		// Variable: 
		String name, place, noun, verb, adj, noun2; 
		int num; 

		// Input:
		System.out.print("Enter a name: ");
		name = scan.nextLine();

		System.out.print("Enter a place: ");
		place = scan.next();

		System.out.print("Enter a nonzero whole number: ");
		num = scan.nextInt();

		System.out.print("Enter a plural noun: ");
		noun = scan.next();

		System.out.print("Enter an adjective: ");
		adj = scan.next();

		System.out.print("Enter a past tense verb: ");
		verb = scan.next();
		

		// Story
		System.out.println(" ");
		System.out.println("The Story of the Walk");
		System.out.println(name + " is walking through the " + place); 		
		System.out.println("As she/he is walking, she/he sees " + noun);
		System.out.println("There are " + num + " of them!");
		System.out.println("The " + noun + " are so " + adj); 
		System.out.println("Suddenly, the " + noun + " turned into wild tigers");
		System.out.println("And what did " + name + " do?");
		System.out.println("He/she " + verb);  	


	}
}
