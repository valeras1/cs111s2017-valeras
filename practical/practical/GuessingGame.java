//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Samantha Valera
// CMPSC 111 Spring 2017
// Practical 06
// Date: 31 March 2017
//
// Purpose: Create a random number from 1 to 100 that the user has to guess
//***********************
import java.util.Date;
import java.util.Scanner;
import java.util.Random;
 
public class GuessingGame
{
	
	public static void main(String[] args)
	{

		System.out.println("Samantha Valera " + new Date() + "\n");

		//Numbers
		final int MAX = 100;
		int answer, guess;

		Scanner scan = new Scanner(System.in);
		Random generator = new Random();
	
		answer = generator.nextInt(MAX) + 1;

		//Asking user for initial input
		System.out.print("I'm thinking of a number between 1 and " + MAX + ". What is my number? ");
		guess = scan.nextInt();
		
		//Statement for when the number is correct 
		if (guess == answer)
			System.out.println("That is correct!");
		else 
		{
			//Statement for when the number is incorrect 
			while (guess != answer)	
			{
				if (answer < guess)
					System.out.println("That's too low! Try again.");
					System.out.print("What is my number? ");
					guess = scan.nextInt();
				else 
				{	
					if (answer > guess)
					System.out.println("That's too high! Try again.");
					System.out.print("What is my number? ");
					guess = scan.nextInt();	
				}
			}
		}

	}
}
