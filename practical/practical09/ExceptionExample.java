//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Samantha Valera
// CMPSC 111 Spring 2017
// Practical 9
// Date: April 28 2017
//***********************
// This example is derived from a Java source code snippet found at:
// http://pages.cs.wisc.edu/~hasti/cs368/JavaTutorial/NOTES/Exceptions.html

public class ExceptionExample {

  //Variable Dictionary
  static void throwsExceptions(int k, int[] A, String S) {
    int j = 1 / k;
    int len = A.length + 1;
    char c;
    
    try {
      c = S.charAt(0);
      if (k == 10) j = A[3];
    }
    catch (ArithmeticException ex) {	// Catches error from divide by 0
      System.out.println("Arithmetic error");
    }
    catch (ArrayIndexOutOfBoundsException ex) {	// Catches error from array
      System.out.println("Array error");
    }
    catch (NullPointerException ex) {	//Catches error from null reference
      System.out.println("Null pointer");
    }
    catch (StringIndexOutOfBoundsException ex) { //Catches error from string out of bounds
      System.out.println("Out of bounds error");
    }
    finally {	//Prints final lines 
      System.out.println("In the finally clause");
    }
    System.out.println("After the try block");
  }

  public static void main(String[] args) {
    int[] X = {0,1,2};
    // List of exceptions
    // throwsExceptions(0, X, "hi");
    // throwsExceptions(10, X, "");
    // throwsExceptions(10, X, "bye");
    // throwsExceptions(10, X, null);  
  }

}
