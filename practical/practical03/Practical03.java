//******************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Taylor Black
// CMPSC 111 Spring 2017
// Practical 03
// Date: 2/03/2017
//
// Purpose: To bug up my own program while de-bugging my partner's program.
//*****************************
import java.util.Date;

public class Practical03
{
  	public static void main(String[] args)
 	 {
     		System.out.println("Taylor Black, CMPSC 111\n" + new Date() + "\n");
      		System.out.println("  __________");
     		System.out.println(" |          \\");
      		System.out.println(" |        0  \\____0");
      		System.out.println(" |  |          ___|  WOOF!");   
      		System.out.println(" |  |_________/");
      		System.out.println(" \\__//");
//The original purpose of the program was to create an "interesting" image by running the program in the terminal window.

//To fix Taylor's bugs, I first compiled the program to see what the terminal returned as errors.
//I noticed what Taylor did was change letters or words, for example, she wrote old Date instead of new Date.
//I also fixed Taylor's spacing because the program would still compile but it was not exactly the correct image. 
	}
}
