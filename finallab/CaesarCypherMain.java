//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Gabriel Herbst and Samantha Valera
// CMPSC 111 Spring 2017
// Final Lab 
// Date: April 20 2017
//
// Purpose: Main method for Caesar Cypher 
//***********************
import java.util.Scanner;

public class CaesarCypherMain{
public static void main(String[] args){
	
	CaesarCypherEncrypt encrypt = new CaesarCypherEncrypt(); //Creates Encrypter 
	CaesarCypherDecrypt decrypt = new CaesarCypherDecrypt(); //Creates Decrypter
	Scanner scan = new Scanner(System.in);
	int task;
	
	System.out.println("Welcome to the Caesar Cypher Encryption/Decryption machine! Enter '1' if you would like to encrypt or '2' if you would like to decrypt.");
	task = scan.nextInt();
	
	if (task == 1) //Conditonal logic to make the program do the correct task
	{
		String cyphertext = encrypt.encrypter(); //Calls Encrypter
		encrypt.printEncryption(cyphertext); 
		System.out.println("");
	}
	else
	{
		String plaintext = decrypt.decrypter(); //Calls Decrypter
		decrypt.printDecryption(plaintext);
		System.out.println("");
	}

	}
}
