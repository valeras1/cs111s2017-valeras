//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Gabriel Herbst and Samantha Valera
// CMPSC 111 Spring 2017
// Final Lab 
// Date: April 20 2017
//
// Purpose: Caesar Cypher Decrypt class 
//***********************
import java.util.*;
public class CaesarCypherDecrypt{

	//Variable Dictionary
	Scanner scan = new Scanner(System.in);
	public String Encryption;
	public int key;
	public final String abc = " abcdefghijklmnopqrstuvwxyz";

	public String decrypter(){

	System.out.println("Please enter the message you would like to decode: ");
		Encryption = scan.nextLine(); //Stores encryption message

		System.out.println("Please enter the key ( a number 1-26)");
		key = scan.nextInt(); //Key for decoding the encrypted method 

		String Decryption = ""; //variable that each decrypted letter is added to
		int newvalue;

		for (int i = 0; i < Encryption.length(); i++) //Begins loop to read each char of message
		{
			int charlocation = abc.indexOf(Encryption.charAt(i)); //Sets each letter of the messaged typed in its string location
			if (charlocation == 0){     // if the char location equals 0 then replacevalue becomes a space
				char replacevalue = abc.charAt(0); 
				Decryption += replacevalue; //adds a space to the Decryption string
					      }
			else {
				newvalue = (charlocation - key) % 27; //Uses key to replace char with decrypted char
			char replacevalue = abc.charAt(newvalue);//Replaces old char with new char
			Decryption += replacevalue;   //adds a new letter to the Decryption string
			     }
		}
		return Decryption; //Returns decrypted to method
	}

public void printDecryption(String Decryption){
char[] string = Decryption.toCharArray(); //Converts Decryption into an array
System.out.println("Your decrypted message is: ");

for(int i = 0; i < Decryption.length(); i++){ //uses a for loop to iterate through the array and print each value
System.out.print( string[i] );}


	}
}

