//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Gabriel Herbst and Samantha Valera
// CMPSC 111 Spring 2017
// Final Lab 
// Date: April 20 2017
//
// Purpose: Caesar Cypher Encrypt class
//***********************

import java.util.*;
import java.io.*;
public class CaesarCypherEncrypt{

	//Variable Dictionary
	Scanner scan = new Scanner(System.in);
	public String message1;
	public int key;
	public final String abc = " abcdefghijklmnopqrstuvwxyz";


	public String encrypter(){


		System.out.println("Please enter the message you would like to encode: ");
		message1 = scan.nextLine(); //String to be encrypted

		System.out.println("Please enter the key ( a number 1-26)");
		key = scan.nextInt(); //Key to encrypt string

		String Encryption = ""; //variable that each encrypted letter is added to
		int newvalue;

		for (int i = 0; i < message1.length(); i++) //Begins to read each char of method
		{
			int charlocation = abc.indexOf(message1.charAt(i)); //Sets each letter for its char location
			if (charlocation == 0)  //if the char location equals 0 then replace value becomes a space
			{
				char replacevalue = abc.charAt(0);
				Encryption += replacevalue; //adds a space to encryption string
			}
			else 
			{
				newvalue = (key + charlocation) % 27; //Uses key to change to a new letter
				char replacevalue = abc.charAt(newvalue); //Replaces new char with old
				Encryption += replacevalue;  //adds the encrypted letter to the encryption variable
			}
		}
		return Encryption; //Returns encryption to method
	}

	public void printEncryption(String Encryption){
		char[] string = Encryption.toCharArray();  //  converts encryption string into an array
System.out.println("Your encrypted code is: " );

		for(int i = 0; i < Encryption.length(); i++){ // uses a for loop to iterate through the array and print out each value
			System.out.print( string[i] );} 


	}
}



