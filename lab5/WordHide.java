//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Samantha Valera and Taylor Black
// CMPSC 111 Spring 2017
// Lab # 5
// Date: February 16 2017
//
// Purpose: Collaboratively Implementing a Steganography Program. 
//***********************

import java.lang.String;
import java.util.Scanner;
import java.util.Random;

public class WordHide  
{
	public static void main(String[] args)
	{
		//Label output with name and date
		System.out.println("Samantha Valera and Taylor Black");
		
		//Variables
		String phrase;
		String mutation1, mutation2, mutation3, mutation4, mutation5, mutation6, mutation7, mutation8, mutation9, mutation10, mutation11, mutation12, mutation13, mutation14, mutation15, mutation16, mutation17, mutation18, mutation19, mutation20, mutation21, mutation22, mutation23, mutation24, mutation25, mutation26, mutation27, mutation28, mutation29;
		int num1, num2, num3, num4, num5, num6, num7, num8, num9; 

		Scanner scan = new Scanner(System.in);
		Random generator = new Random();

		// User Input
		System.out.println("Please enter a word:");
		phrase = scan.nextLine();
		String padded = phrase + ("012345678");
			
		// Mutations to change letters entered to numbers 		
		mutation1 = padded.toUpperCase();
		mutation2 = mutation1.replace('A', '0');
		mutation3 = mutation2.replace('B', '1');
		mutation4 = mutation3.replace('C', '2');
		mutation5 = mutation4.replace('D', '3');
		mutation6 = mutation5.replace('E', '4');
		mutation7 = mutation6.replace('F', '5');
		mutation8 = mutation7.replace('G', '6');
		mutation9 = mutation8.replace('H', '7');
		mutation10 = mutation9.replace('I', '8');
		mutation11 = mutation10.replace('J', '9');
		mutation12 = mutation11.replace('K', '0');
		mutation13 = mutation12.replace('L', '1');
		mutation14 = mutation13.replace('M', '2');
		mutation15 = mutation14.replace('N', '3');
		mutation16 = mutation15.replace('O', '4');
		mutation17 = mutation16.replace('P', '5');
		mutation18 = mutation17.replace('Q', '6');
		mutation19 = mutation18.replace('R', '7');
		mutation20 = mutation19.replace('S', '8');
		mutation21 = mutation20.replace('T', '9');
		mutation22 = mutation21.replace('U', '0');
		mutation23 = mutation22.replace('V', '1');
		mutation24 = mutation23.replace('W', '2');
		mutation25 = mutation24.replace('X', '3');
		mutation26 = mutation25.replace('Y', '4');
		mutation27 = mutation26.replace('Z', '5');
		mutation28 = mutation27.replace(' ', '6');
		mutation29 = mutation28.substring(0, 10);
		
		// Generatoring random numbers 
		num1 = generator.nextInt(1000000000) + 1000000000;
		num2 = generator.nextInt(1000000000) + 1000000000;
		num3 = generator.nextInt(1000000000) + 1000000000;
		num4 = generator.nextInt(1000000000) + 1000000000;
		num5 = generator.nextInt(1000000000) + 1000000000;
		num6 = generator.nextInt(1000000000) + 1000000000;
		num7 = generator.nextInt(1000000000) + 1000000000;
		num8 = generator.nextInt(1000000000) + 1000000000;
		num9 = generator.nextInt(1000000000) + 1000000000;
		
		// Printing out 20x20 grid				
		System.out.println(mutation29 + (num1));
		System.out.println((num2) + mutation29);
		System.out.println(mutation29 + (num3));
		System.out.println((num4) + mutation29);
		System.out.println(mutation29 + (num5));
		System.out.println((num6) + mutation29);
		System.out.println(mutation29 + (num7));
		System.out.println((num8) + mutation29);
		System.out.println(mutation29 + (num9));
		System.out.println((num5) + mutation29);
		System.out.println(mutation29 + (num4));
		System.out.println((num3) + mutation29);
		System.out.println(mutation29 + (num2));
		System.out.println((num1) + mutation29);
		System.out.println(mutation29 + (num8));
		System.out.println((num9) + mutation29);
		System.out.println(mutation29 + (num7));
		System.out.println((num5) + mutation29);
		System.out.println(mutation29 + (num4));
		System.out.println((num8) + mutation29);	
		
	}
}
