/* ***************************************************************
The work we are submitting is a result of our own thinking and efforts
Sam Valera and Josh Greschler
CMPSC 111 Spring 2017
Lab 7
Date: 3/2/17

Pourpse: to calculate various geometric properties given user input
*************************************************************** */

//imports the date and scanner classes to be used in the body of the program
import java.util.Date;
import java.util.Scanner;

public class CommandLineGeometer {

    //Allows GeometricShape to be three different strings
    private enum GeometricShape { sphere, triangle, cylinder };

    public static void main(String[] args) {
	Scanner scan = new Scanner(System.in);
        
	GeometricShape shape = GeometricShape.sphere;
        
	//declares variable types before input
	double radius;
        double height;
        int x;
        int y;
        int z;
	int j;
	//prints header
        System.out.println("Sam Valera and Josh Greschler " + new Date());
        
	System.out.println("Welcome to the Command Line Geometer!");
        System.out.println();

	//asks user for value then uses the scanner class to save it to the variable radius
        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();


        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        
	//calls the program geometric calculator to use the 'calculateSphereVolume' method using the argument 'radius'
	double sphereVolume = GeometricCalculator.calculateSphereVolume(radius);
        System.out.println("The volume is equal to " + sphereVolume);
        System.out.println();

	//uses the 'calculateSphereSurfaceArea' method in GeometricCalculator
	double sphereSurfaceArea = GeometricCalculator.calculateSphereSurfaceArea(radius);
	System.out.println("The surface area is equal to " + sphereSurfaceArea);

	//changes shape to triangle
        shape = GeometricShape.triangle;

	//user inputs the lengths of the three sides of a triangle, which are stored as x, y, and z
        System.out.println("What is the length of the first side?");
        x = scan.nextInt();

        System.out.println("What is the length of the second side?");
        y = scan.nextInt();

        System.out.println("What is the length of the third side?");
        z = scan.nextInt();
        System.out.println();

        System.out.println("Calculating the area of a " + shape);
	
	//uses a new method in the GeometricCalculator to calculate the area. passes the values of x, y, and z to the method.
        double triangleArea = GeometricCalculator.calculateTriangleArea(x, y, z);
        System.out.println("The area is equal to " + triangleArea);
        System.out.println();

	//changes shape to cylinder
        shape = GeometricShape.cylinder;

	//asks user for inputs to store as radius and height
        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();

        System.out.println("What is the height for the " + shape + "?");
        height = scan.nextDouble();
        System.out.println();

	
        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius + " and height equal to " + height);

	//uses a new method in the GeometricCalculator
        double cylinderVolume = GeometricCalculator.calculateCylinderVolume(radius, height);
        System.out.println("The volume is equal to " + cylinderVolume);
        System.out.println();
	double cylinderSurfaceArea = GeometricCalculator.calculateCylinderSurfaceArea(radius, height);
	System.out.println("The surface area is equal to " + cylinderSurfaceArea);
    }
}
