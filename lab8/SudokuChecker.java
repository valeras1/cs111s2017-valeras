//***********************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Samantha Valera and Natasha Torrence 
// CMPSC 111 Spring 2017
// Lab #8
// Date: March 9 2017 
//
// Purpose: Sudoku Puzzle
//***********************
import java.util.Date; 
import java.util.Scanner;

public class SudokuChecker
{
	Scanner scan = new Scanner(System.in);

		//These are our sudoku values
		private int w1, w2, w3, w4, x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4;
		
		//This is our constructor
		public SudokuChecker() 
		{	
			w1 = 0;
			w2 = 0;
			w3 = 0;
			w4 = 0;
			x1 = 0;
			x2 = 0;
			x3 = 0;
			x4 = 0;
			y1 = 0;
			y2 = 0;
			y3 = 0;
			y4 = 0;
			z1 = 0;
			z2 = 0;
			z3 = 0;
			z4 = 0;
		}
		
		public void inputGridRow()
		{
			//This is row 1
			System.out.print("Enter Row 1: ");
			w1 = scan.nextInt();
			w2 = scan.nextInt();
			w3 = scan.nextInt();
			w4 = scan.nextInt();
			System.out.println("You entered " + w1 + " " + w2 + " " + w3 + " " + w4 + " for you first row.");

			//This is row 2
			System.out.print("Enter Row 2: ");
			x1 = scan.nextInt();
			x2 = scan.nextInt();
			x3 = scan.nextInt();
			x4 = scan.nextInt();
			System.out.println("You entered " + x1 + " " + x2 + " " + x3 + " " + x4 + " for you second row.");
			
			//This is row 3
			System.out.print("Enter Row 3: ");
			y1 = scan.nextInt();
			y2 = scan.nextInt();
			y3 = scan.nextInt();
			y4 = scan.nextInt();
			System.out.println("You entered " + y1 + " " + y2 + " " + y3 + " " + y4 + " for you third row.");

			//This is row 4
			System.out.print("Enter Row 4: ");
			z1 = scan.nextInt();	
			z2 = scan.nextInt();
			z3 = scan.nextInt();
			z4 = scan.nextInt();
			System.out.println("You entered " + z1 + " " + z2 + " " + z3 + " " + z4 + " for you fourth row.");	
			   
		}
		
   
		//This checks our puzzle
		public void checkGrid()
		{
			//This checks row 1
			if ((w1 + w2 + w3 + w4) == 10) {
				System.out.println("This row is good");}
			else {System.out.println("This is invalid");}
			
			//This checks row 2
			if ((x1 + x2 + x3 + x4) == 10) {
				System.out.println("This row is good");}
			else {System.out.println("This is invalid");}
			
			//this checks row 3
			if ((y1 + y2 + y3 + y4) == 10) {
				System.out.println("This row is good");}
			else {System.out.println("This is invalid");}

			//this checks row 4
			if ((z1 + z2 + z3 + z4) == 10) {
				System.out.println("This row is good");}
			else {System.out.println("This is invalid");}
		
			//this checks column 1
			if ((w1 + x1 + y1 + z1) == 10) {
				System.out.println("This column is good");}
			else {System.out.println("This is invalid");}

			//this checks column 2
			if ((w2 + x2 + y2 + z2) == 10) {
				System.out.println("This column is good");}
			else {System.out.println("This is invalid");}

			//this checks column 3
			if ((w3 + x3 + y3 + z3) == 10) {
				System.out.println("This column is good");}
			else {System.out.println("This is invalid");}

			//this check column 4
			if ((w4 + x4 + y4 + z4) == 10) {
				System.out.println("This column is good");}
			else {System.out.println("This is invalid");}
			
			//checks region 1
			if ((w1 + w2 + x1 + x2) == 10) {
				System.out.println("This region is good");}
			else {System.out.println("This is invalid");}
			
			//checks region 2
			if ((w3 + w4 + x3 + x4) == 10) {
				System.out.println("This region is good");}
			else {System.out.println("This is invalid");}

			//checks region 3
			if ((y1 + y2 + z1 + z2) == 10) {
				System.out.println("This region is good");}
			else {System.out.println("This is invalid");}

			//checks region 4
			if ((y3 + y4 + z3 + z4) == 10) {
				System.out.println("This region is good");}
			else {System.out.println("This is invalid");}

						
		}
						
}

