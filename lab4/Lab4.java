//=================================================
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Samantha Valera
// CMPSC 111 Spring 2017
// Lab #4
// Date: February 09 2017
//
// Purpose: To gain more experience using graphics methods in Java.
//=================================================

import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
  //-------------------------------------------------
  // Use Graphics methods to draw flowers.
  //-------------------------------------------------
  public void paint(Graphics page)
  {
	final int WIDTH = 600;
	final int HEIGHT = 400;

	page.setColor(Color.blue); // Sky
	page.fillRect(0,0,WIDTH, HEIGHT);
	
	page.setColor(Color.green); // Ground
 	page.fillRect(0,HEIGHT - 80, WIDTH, HEIGHT);

	page.setColor(Color.orange); // Plot of Dirt 1
	page.fillRect(320, 320, 100, 50);

	page.setColor(Color.green); // Stem 1
	page.fillRect(355, 250, 5, 100);	

	page.setColor(Color.yellow); // Center of Flower 1
	page.fillOval(345, 249, 20, 20);

	page.setColor(Color.magenta); // Petals 1
	page.fillOval(360, 249, 30, 25); 
	page.fillOval(345, 260, 30, 25); 
	page.fillOval(360, 240, 30, 25);
	page.fillOval(335, 235, 30, 25);
	page.fillOval(325, 260, 30, 25);
	page.fillOval(320, 249, 30, 25);
	
	page.setColor(Color.orange); // Plot of Dirt 2
	page.fillRect(100, 320, 100, 50);	

	page.setColor(Color.green); // Stem 2
	page.fillRect(135, 250, 5, 100);	

	page.setColor(Color.pink); // Center of Flower 2
	page.fillOval(125, 249, 20, 20);

	page.setColor(Color.red); // Petals 2
	page.fillOval(140, 249, 30, 25);
	page.fillOval(125, 260, 30, 25);
	page.fillOval(140, 240, 30, 25);
	page.fillOval(115, 235, 30, 25);
	page.fillOval(105, 260, 30, 25);
	page.fillOval(100, 249, 30, 25);
	
	page.drawString("Be Your Own Garden", 200, 200); //Random Quote

	  }
}
