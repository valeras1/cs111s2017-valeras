import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

// NOTE: You do not need to understand all of the working details associated with this program.
// Instead, you should develop a basic understanding of how it works. Please consult with a
// teaching assistant, tutor, or a course instructor if there are parts of the program that you
// do not intuitively understand.

public class MandelbrotBW {
    public static void main(String[] args) throws Exception {
        int width = 1920, height = 1080, max = 1000;	// size of image
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        int black = 0x000000, white = 0xFFFFFF;	// setting the image as black and white

        for (int row = 0; row < height; row++) {	//for loop for row 0, adding one as long as height was greater than row
            for (int col = 0; col < width; col++) {	//for loop for column 0, adding one as long as width was greather than column 
                // declaring variables
		double c_re = (col - width/2)*4.0/width;
                double c_im = (row - height/2)*4.0/width;
                double x = 0, y = 0;
                int iterations = 0;
		//while loop
                while (x*x+y*y < 4 && iterations < max) {
                    double x_new = x*x-y*y+c_re;
                    y = 2*x*y+c_im;
                    x = x_new;
                    iterations++;
                }
		//colors row and column as white as long as the max is greater than iteration
                if (iterations < max) image.setRGB(col, row, white);
		//if above not satisfied, image will be colored in black
                else image.setRGB(col, row, black);
            }
        }
	//saves image
        ImageIO.write(image, "png", new File("mandelbrot-bw.png"));
    }
}
